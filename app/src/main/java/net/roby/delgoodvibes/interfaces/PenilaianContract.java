package net.roby.delgoodvibes.interfaces;

import net.roby.delgoodvibes.models.Penilaian;

public class PenilaianContract {
    public interface View{
        void responseToSavedPenilaian(Penilaian penilaian);

    }

    public interface Controller{
        void submitPenilaian(Penilaian penilaian);
        void returnedPenilaian(Penilaian penilaian);

    }

    public interface  Repository{
        void savePenilaian(Penilaian penilaian);

    }
}

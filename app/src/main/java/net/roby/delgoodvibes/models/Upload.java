package net.roby.delgoodvibes.models;

public class Upload {
    private String name;
    private String imageUri;
    private String userId;

    public Upload() {
    }

    public Upload(String name, String imageUri, String userId) {
        if(name.trim().equals("")){
            name = "No Name";
        }
        this.name = name;
        this.imageUri = imageUri;
        this.userId = userId;
    }

    public void setUserId(String mUserId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getUserId() {
        return userId;
    }
}

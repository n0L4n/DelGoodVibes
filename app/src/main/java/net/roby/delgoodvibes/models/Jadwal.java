package net.roby.delgoodvibes.models;

import java.util.Date;

public class Jadwal {
    private String hari;
    private Date jamMasuk;
    private Date jamKeluar;
    private String[] currKelas;
    private String[] prevKelas;
    private String fasilitasId;

    public Jadwal() {
    }

    public Jadwal(String hari, Date jamMasuk, Date jamKeluar, String[] currKelas, String[] prevKelas, String fasilitasId) {
        this.hari = hari;
        this.jamMasuk = jamMasuk;
        this.jamKeluar = jamKeluar;
        this.currKelas = currKelas;
        this.prevKelas = prevKelas;
        this.fasilitasId = fasilitasId;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public Date getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(Date jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public Date getJamKeluar() {
        return jamKeluar;
    }

    public void setJamKeluar(Date jamKeluar) {
        this.jamKeluar = jamKeluar;
    }

    public String[] getCurrKelas() {
        return currKelas;
    }

    public void setCurrKelas(String[] currKelas) {
        this.currKelas = currKelas;
    }

    public String[] getPrevKelas() {
        return prevKelas;
    }

    public void setPrevKelas(String[] prevKelas) {
        this.prevKelas = prevKelas;
    }

    public String getFasilitasId() {
        return fasilitasId;
    }

    public void setFasilitasId(String fasilitasId) {
        this.fasilitasId = fasilitasId;
    }
}

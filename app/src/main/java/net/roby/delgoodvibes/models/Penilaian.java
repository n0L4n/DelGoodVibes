package net.roby.delgoodvibes.models;

import java.util.Date;

public class Penilaian {
    private String userId;
    private String waktu;
    private int nilaiKebersihan;
    private int nilaiKerapihan;
    private String fasilitasId;

    public Penilaian() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public int getNilaiKebersihan() {
        return nilaiKebersihan;
    }

    public void setNilaiKebersihan(int nilaiKebersihan) {
        this.nilaiKebersihan = nilaiKebersihan;
    }

    public int getNilaiKerapihan() {
        return nilaiKerapihan;
    }

    public void setNilaiKerapihan(int nilaiKerapihan) {
        this.nilaiKerapihan = nilaiKerapihan;
    }

    public String getFasilitasId() {
        return fasilitasId;
    }

    public void setFasilitasId(String fasilitasId) {
        this.fasilitasId = fasilitasId;
    }
}

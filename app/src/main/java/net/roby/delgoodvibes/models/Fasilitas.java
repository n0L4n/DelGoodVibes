package net.roby.delgoodvibes.models;

public class Fasilitas {
    private String namaFasilitas;
    private String idFasilitas;
    private double ratingKebersihan;
    private double ratingKerapian;
    private String desc;

    public Fasilitas() {
    }

    public Fasilitas(String namaFasilitas, String idFasilitas, double ratingKebersihan, double ratingKerapian, String descFasilitas) {
        this.namaFasilitas = namaFasilitas;
        this.idFasilitas = idFasilitas;
        this.ratingKebersihan = ratingKebersihan;
        this.ratingKerapian = ratingKerapian;
        this.desc = descFasilitas;
    }

    public String getNamaFasilitas() {
        return namaFasilitas;
    }

    public void setNamaFasilitas(String namaFasilitas) {
        this.namaFasilitas = namaFasilitas;
    }

    public String getIdFasilitas() {
        return idFasilitas;
    }

    public void setIdFasilitas(String idFasilitas) {
        this.idFasilitas = idFasilitas;
    }

    public double getRatingKebersihan() {
        return ratingKebersihan;
    }

    public void setRatingKebersihan(double ratingKebersihan) {
        this.ratingKebersihan = ratingKebersihan;
    }

    public double getRatingKerapian() {
        return ratingKerapian;
    }

    public void setRatingKerapian(double ratingKerapian) {
        this.ratingKerapian = ratingKerapian;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String descFasilitas) {
        this.desc = descFasilitas;
    }
}

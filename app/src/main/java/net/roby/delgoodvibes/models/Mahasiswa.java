package net.roby.delgoodvibes.models;

public class Mahasiswa {
    private String userId;
    private String nama;
    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String userId, String nama, String nim) {
        this.userId = userId;
        this.nama = nama;
        this.nim = nim;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
}

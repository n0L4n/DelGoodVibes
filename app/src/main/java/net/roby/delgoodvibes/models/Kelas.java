package net.roby.delgoodvibes.models;

public class Kelas {
    private String idKelas;
    private String namaKelas;
    private double ratingKebersihan;
    private double ratingKerapian;

    public Kelas() {
    }

    public Kelas(String idKelas, String namaKelas, double ratingKebersihan, double ratingKerapian) {
        this.idKelas = idKelas;
        this.namaKelas = namaKelas;
        this.ratingKebersihan = ratingKebersihan;
        this.ratingKerapian = ratingKerapian;
    }

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getNamaKelas() {
        return namaKelas;
    }

    public void setNamaKelas(String namaKelas) {
        this.namaKelas = namaKelas;
    }

    public double getRatingKebersihan() {
        return ratingKebersihan;
    }

    public void setRatingKebersihan(double ratingKebersihan) {
        this.ratingKebersihan = ratingKebersihan;
    }

    public double getRatingKerapian() {
        return ratingKerapian;
    }

    public void setRatingKerapian(double ratingKerapian) {
        this.ratingKerapian = ratingKerapian;
    }
}

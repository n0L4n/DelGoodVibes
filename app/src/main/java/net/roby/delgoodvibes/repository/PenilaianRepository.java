package net.roby.delgoodvibes.repository;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.roby.delgoodvibes.interfaces.PenilaianContract;
import net.roby.delgoodvibes.models.Fasilitas;
import net.roby.delgoodvibes.models.Penilaian;

public class PenilaianRepository implements PenilaianContract.Repository{
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabaseRef;
    private FirebaseAuth firebaseAuth;
    private final PenilaianContract.Controller mController ;


    public PenilaianRepository(PenilaianContract.Controller mController) {
        this.mController = mController;
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseRef = mDatabase.getReference("penilaian");
    }

    @Override
    public void savePenilaian(Penilaian penilaian) {
        //updateFasilitas(penilaian.getFasilitasId());
        String id = mDatabaseRef.push().getKey();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        penilaian.setUserId(user.getUid());
        mDatabaseRef.child(id).setValue(penilaian);
        mController.returnedPenilaian(penilaian);
    }

    private void updateFasilitas(String fasilitas){
        System.out.println("masuk + "+fasilitas);
        DatabaseReference fasilitasRef = mDatabase.getReference("fasilitas");
        fasilitasRef.equalTo(fasilitas)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        System.out.println("data change");
                        System.out.println(dataSnapshot.toString());
                        Fasilitas f = (Fasilitas) dataSnapshot.getValue();
                        System.out.println("nama : "+f.getNamaFasilitas());
                        System.out.println("rating kebersihan "+f.getRatingKebersihan());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



    }
}

package net.roby.delgoodvibes.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import net.roby.delgoodvibes.R;

public class DashboardActivity extends AppCompatActivity {
    public static final int BERI_PENILAIAN_REQUEST_CODE=1;


    private CardView menuPenilaian;
    private CardView menuUpload;
    private CardView menuRankingKelas;
    private CardView menuRankingFasilitas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initVariables();
    }

    private void initVariables(){
        menuPenilaian = (CardView) findViewById(R.id.beriPenilaian);
        menuUpload = (CardView) findViewById(R.id.upload);
        menuRankingKelas = (CardView) findViewById(R.id.rankingKelas);
        menuRankingFasilitas = (CardView) findViewById(R.id.rankingFasilitas);

        menuPenilaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("CLICKED");
                startActivityForResult(new Intent(DashboardActivity.this,PenilaianActivity.class),BERI_PENILAIAN_REQUEST_CODE);
            }
        });

        menuUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // finish();
                startActivity(new Intent(DashboardActivity.this, ProfileActivity.class));
            }
        });

        menuRankingKelas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  finish();
                startActivity(new Intent(DashboardActivity.this, KelasActivity.class));
            }
        });

        menuRankingFasilitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // finish();
                startActivity(new Intent(DashboardActivity.this,FasilitasActivity.class));
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BERI_PENILAIAN_REQUEST_CODE && resultCode == RESULT_OK){
            Toast.makeText(this,"Penilaian telah disubmit",Toast.LENGTH_SHORT).show();
        }
    }
}

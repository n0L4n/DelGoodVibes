package net.roby.delgoodvibes.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.roby.delgoodvibes.R;
import net.roby.delgoodvibes.models.Upload;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private Context mContext;
    private List<Upload> mUploads;
    private Upload tampUpload;
    private ImageViewHolder tempHolder;

    public ImageAdapter(Context context, List<Upload> uploads){
        mContext = context;
        mUploads = uploads;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.image_item,parent,false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        Upload uploadCurr = mUploads.get(position);
        this.tampUpload = uploadCurr;
        holder.textViewName.setText(uploadCurr.getName());
        tempHolder = holder;
        Picasso.with(mContext)
                .load(uploadCurr.getImageUri())
                .fit()
                .centerCrop()
                .into(holder.imageView,new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        System.out.println("Sukses "+tampUpload.getImageUri());
                    }

                    @Override
                    public void onError() {
                        System.out.println("Gagal : "+tampUpload.getImageUri());

                    }
                });
    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName;
        public ImageView imageView;

        public ImageViewHolder(View viewItem){
            super(viewItem);

            textViewName = itemView.findViewById(R.id.textViewName);
            imageView = itemView.findViewById(R.id.imageViewUpload);
        }
    }
}

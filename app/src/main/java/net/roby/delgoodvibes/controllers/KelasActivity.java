package net.roby.delgoodvibes.controllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import net.roby.delgoodvibes.R;
import net.roby.delgoodvibes.models.Kelas;
import net.roby.delgoodvibes.models.Kelas;

public class KelasActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Kelas,KelasActivity.KelasViewHolder> mRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelas);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("kelas");
        mDatabase.keepSynced(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.myRecycleView);


        DatabaseReference KelasRef = FirebaseDatabase.getInstance().getReference().child("kelas");
        Query KelasQuery = KelasRef.orderByChild("ratingKebersihan");

        mRecyclerView.hasFixedSize();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Kelas> KelasOptions = new FirebaseRecyclerOptions.Builder<Kelas>().setQuery(KelasQuery,Kelas.class).build();

        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<Kelas, KelasActivity.KelasViewHolder>(KelasOptions){
            @Override
            public void onBindViewHolder(KelasActivity.KelasViewHolder holder,final int position, final Kelas model) {
                System.out.println("nama : "+model.getNamaKelas());
                holder.setRowNumber(Integer.toString(position+1));
                holder.setNama(model.getNamaKelas());
                holder.setKebersihan(model.getRatingKebersihan());
                holder.setKerapian(model.getRatingKerapian());
            }

            @Override
            public KelasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.kelas_row,parent,false);
                return new KelasActivity.KelasViewHolder(view);
            }
        };
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRecyclerViewAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRecyclerViewAdapter.stopListening();
    }

    public static class KelasViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public KelasViewHolder(View itemView){
            super(itemView);
            mView = itemView;
        }

        public void setRowNumber(String num){
            TextView rowNumber = (TextView) mView.findViewById(R.id.row_num);
            rowNumber.setText(num);
        }

        public void setNama(String nama){
            TextView namaKelas = (TextView) mView.findViewById(R.id.nama_kelas);
            namaKelas.setText(nama);
        }

        public void setDesc(String desc){
            TextView deskripsi = (TextView) mView.findViewById(R.id.desc_kelas);
            deskripsi.setText(desc);
        }

        public void setKebersihan(double rate){
            TextView kebersihan = (TextView) mView.findViewById(R.id.rating_kebersihan);
            rate=10*rate;
            kebersihan.setText("Rating Kebersihan : "+rate+"%");
        }

        public void setKerapian(double rate){
            TextView kerapian = (TextView) mView.findViewById(R.id.rating_kerapian);
            rate=10*rate;
            kerapian.setText("Rating Kerapian : "+rate+"%");
        }
    }
}

package net.roby.delgoodvibes.controllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.roby.delgoodvibes.R;
import net.roby.delgoodvibes.controllers.controllers.PenilaianController;
import net.roby.delgoodvibes.interfaces.PenilaianContract;
import net.roby.delgoodvibes.models.Penilaian;
import net.roby.delgoodvibes.utils.Utility;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PenilaianActivity extends AppCompatActivity implements PenilaianContract.View {
    private final PenilaianContract.Controller mController = new PenilaianController(this);
    private Penilaian submittedPenilaian;

    private SeekBar seekBarKebersihan;
    private SeekBar seekBarKerapihan;
    private TextView txtKebersihan;
    private TextView txtKerapihan;
    private Spinner fasilitas;
    private String selectedFasilitas;
    private Button submit;

    private DatabaseReference mDatabaseRef;

    private Map<String,String> listMapFasilitas;
    private List<String> listFasilitas;

    private int nilaiKebersihan;
    private int nilaiKerapian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penilaian);
        initializeVariables();
        initSpinner();


        // Initialize the textview with '0'.
        txtKebersihan.setText("Nilai : " + seekBarKebersihan.getProgress() + "/" + seekBarKebersihan.getMax());
        txtKerapihan.setText("Nilai : " + seekBarKerapihan.getProgress() + "/" + seekBarKerapihan.getMax());

        seekBarKebersihan.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                txtKebersihan.setText("Nilai : " + progress + "/" + seekBar.getMax());
                // Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                txtKebersihan.setText("Nilai: " + progress + "/" + seekBar.getMax());
                //   Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txtKebersihan.setText("Nilai: " + progress + "/" + seekBar.getMax());
                //  Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
                nilaiKebersihan = progress;
            }
        });

        seekBarKerapihan.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                txtKerapihan.setText("Nilai : " + progress + "/" + seekBar.getMax());
                // Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                txtKerapihan.setText("Nilai: " + progress + "/" + seekBar.getMax());
                //   Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txtKerapihan.setText("Nilai: " + progress + "/" + seekBar.getMax());
                //  Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
                nilaiKerapian = progress;
            }
        });

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.fasilitas_array, android.R.layout.simple_spinner_item);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        fasilitas.setAdapter(adapter);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String timeStamp = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss").format(new Date());
                System.out.println("waktu : "+timeStamp);
                Penilaian penilaian = new Penilaian();
                penilaian.setFasilitasId(selectedFasilitas);
                penilaian.setNilaiKebersihan(nilaiKebersihan);
                penilaian.setNilaiKerapihan(nilaiKerapian);
                penilaian.setWaktu(timeStamp);
                mController.submitPenilaian(penilaian);

            }
        });

    }

    private void initSpinner(){
        System.out.println("Init Spinner");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                listMapFasilitas.put(dataSnapshot.child("namaFasilitas").getValue(String.class),dataSnapshot.child("idFasilitas").getValue(String.class) );
                listFasilitas.add(dataSnapshot.child("namaFasilitas").getValue(String.class));
                System.out.println("Fasiilitas "+dataSnapshot.child("namaFasilitas").getValue(String.class));
                ArrayAdapter<String> listFasilitasAdapter = new ArrayAdapter<String>(PenilaianActivity.this, android.R.layout.simple_spinner_item,listFasilitas);
                listFasilitasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                fasilitas.setAdapter(listFasilitasAdapter);
                System.out.println("OnItemSelected");
                fasilitas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        //selectedFasilitas = listMapFasilitas.get((String) adapterView.getItemAtPosition(i));
                        selectedFasilitas = (String) adapterView.getItemAtPosition(i);
                        System.out.println("Selected "+selectedFasilitas);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        System.out.println("Nothing selected");
                    }
                });
                System.out.println("OnItemSelected Closed");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mDatabaseRef.addChildEventListener(childEventListener);


        System.out.println("Adapter");




    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.

        //getMenuInflater().inflate(R.mipmap.ic_launcher, menu);

        return true;

    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will

        // automatically handle clicks on the Home/Up button, so long

        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.submit) {

            return true;

        }

        return super.onOptionsItemSelected(item);

    }




    // A private method to help us initialize our variables.
    private void initializeVariables() {
        seekBarKebersihan = (SeekBar) findViewById(R.id.seekBarKebersihan);
        txtKebersihan = (TextView) findViewById(R.id.txtKebersihan);
        seekBarKerapihan = (SeekBar) findViewById(R.id.seekBarKerapihan);
        txtKerapihan = (TextView) findViewById(R.id.txtKerapihan);
        fasilitas = (Spinner) findViewById(R.id.fasilitas);
        fasilitas.setPrompt("Pilih Fasilitas");
        submit = (Button) findViewById(R.id.submit);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference("fasilitas");
        listMapFasilitas = new HashMap<>();
        listFasilitas = new ArrayList<String>();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void responseToSavedPenilaian(Penilaian penilaian) {

        setResult(RESULT_OK);
        finish();
    }
}

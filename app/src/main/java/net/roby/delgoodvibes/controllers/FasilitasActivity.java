package net.roby.delgoodvibes.controllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import net.roby.delgoodvibes.R;
import net.roby.delgoodvibes.models.Fasilitas;

public class FasilitasActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Fasilitas,FasilitasActivity.FasilitasViewHolder> mRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fasilitas);

        setTitle("Ranking Fasilitas");

        mDatabase = FirebaseDatabase.getInstance().getReference().child("fasilitas");
        mDatabase.keepSynced(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.myRecycleView);


        DatabaseReference fasilitasRef = FirebaseDatabase.getInstance().getReference().child("fasilitas");
        Query fasilitasQuery = fasilitasRef.orderByChild("ratingKebersihan");

        mRecyclerView.hasFixedSize();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Fasilitas> fasilitasOptions = new FirebaseRecyclerOptions.Builder<Fasilitas>().setQuery(fasilitasQuery,Fasilitas.class).build();

        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<Fasilitas, FasilitasActivity.FasilitasViewHolder>(fasilitasOptions){
            @Override
            public void onBindViewHolder(FasilitasActivity.FasilitasViewHolder holder,final int position, final Fasilitas model) {
                System.out.println("nama : "+model.getNamaFasilitas());
                holder.setRowNumber(Integer.toString(position+1));
                holder.setNama(model.getNamaFasilitas());
                holder.setDesc(model.getDesc());
                holder.setKebersihan(model.getRatingKebersihan());
                holder.setKerapian(model.getRatingKerapian());
            }

            @Override
            public FasilitasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fasilitas_row,parent,false);
                return new FasilitasActivity.FasilitasViewHolder(view);
            }
        };
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRecyclerViewAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRecyclerViewAdapter.stopListening();
    }

    public static class FasilitasViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public FasilitasViewHolder(View itemView){
            super(itemView);
            mView = itemView;
        }

        public void setRowNumber(String num){
            TextView rowNumber = (TextView) mView.findViewById(R.id.row_num);
            rowNumber.setText(num);
        }

        public void setNama(String nama){
            TextView namaFasilitas = (TextView) mView.findViewById(R.id.nama_fasilitas);
            namaFasilitas.setText(nama);
        }

        public void setDesc(String desc){
            TextView deskripsi = (TextView) mView.findViewById(R.id.desc_fasilitas);
            deskripsi.setText(desc);
        }

        public void setKebersihan(double rate){
            TextView kebersihan = (TextView) mView.findViewById(R.id.rating_kebersihan);
            rate=10*rate;
            kebersihan.setText("Rating Kebersihan : "+rate+"%");
        }

        public void setKerapian(double rate){
            TextView kerapian = (TextView) mView.findViewById(R.id.rating_kerapian);
            rate=10*rate;
            kerapian.setText("Rating Kerapian : "+rate+"%");
        }
    }
}

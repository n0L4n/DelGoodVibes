package net.roby.delgoodvibes.controllers.controllers;

import net.roby.delgoodvibes.interfaces.PenilaianContract;
import net.roby.delgoodvibes.models.Penilaian;
import net.roby.delgoodvibes.repository.PenilaianRepository;

public class PenilaianController implements PenilaianContract.Controller{
    private final PenilaianContract.View mView;
    private final PenilaianContract.Repository mRepo = new PenilaianRepository(this);

    public PenilaianController(PenilaianContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void submitPenilaian(Penilaian penilaian) {
        mRepo.savePenilaian(penilaian);
    }

    @Override
    public void returnedPenilaian(Penilaian penilaian) {

        mView.responseToSavedPenilaian(penilaian);
    }
}
